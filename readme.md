## Table of contents
* [General info](#general-info)
* [Setup](#setup)


## General info
This project aims to connect gait informations given by the treadmill, force plates to the neuromusculoskeletal model.
The software CEINMS-RT can access the information through the ADS pool. The Master pc will run CEINMS and TWINCAT, which connects to three slaves: the treadmill, the trigger box and the nucleo Board.
The neuromusculoskeletal model would need information about the gait, such as the gait phase and the speed of the gait at each timestamp, in order to compute joint torques.
The output would be then sent to the Empower as reference torque.
The simulink model ( tmp, repo Simulink_synergies_twincat_speedControl ) generates gait phase estimation using GRFs data from the force plate. The OUTPUT Phase_out is a vector that contains values for left and right in position 0 and 1 respectively.This is accessed by CEINMS-RT.
In addition you find a module to control the velocity of the treadmill.
The simulink module can reproduce a ramp signal to reach a value maxVel .The output velocity is the one reached by the two belts of the treadmill that receive the voltage output from ethercat.

Also there is a randomsignal output used for synchronization for the current project that is used with CEINMS-RT. (Check plugin synergy-model)
A PLC module ( simbionics-twincat-master ) is added to control the Empower, it is linked to the Nucleo Board which is added as a slave in the project.
You have a measurement project paired to this one that can be used to record and display your signals during the experiment.
Also there is an option from the simulink project that saves the data in a .mat format. 
## Setup
To run this project:

It is necessary to connect to the forceplate via ethercat and make sure the cables for the speed control are also connected ( find practical info on confluence-https://be.et.utwente.nl/confluence/display/EQSE/Analog+control+speed+treadmill )

then you need to scan the new device.
link the output of the voltages following the instructions : P1.3 to the left belt and P1.4 to the right belt.

When you activate the simulation in running mode make sure the gains for left and right forces are set to 1 inside the stride_estimator block. set the reset_phase_estimator to 1 at the beginning of the experiment to calibrate the module.

in the speed control module you have a maximum velocity and an offset which reprent the end point and the starting point. MaxVel is the maximum slope of the ramp that would be reproduced to reach the maximum speed.
The reset buttom brings the treadmill back to the offset velocity.
NB Make sure you have the licence for simulink-beckoff active on your computer!
NB make sure you rename the output file in .mat everytime you run the project. first stop the run and then restert it .






